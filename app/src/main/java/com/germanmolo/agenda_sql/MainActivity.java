package com.germanmolo.agenda_sql;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import database.AgendaContactos;
import database.AgendaDBHelper;
import database.Contacto;


public class MainActivity extends AppCompatActivity {

    private EditText txtNombre;
    private EditText txtTelefono1;
    private EditText txtTelefono2;
    private EditText txtDireccion;
    private EditText txtNotas;
    private CheckBox cbFavorito;
    private Button btnAgregar;
    private Button btnListar;
    private Button btnLimpiar;
    private AgendaContactos db;
    private Contacto savedContact;
    private int fav;
    private long id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtTelefono1 = (EditText) findViewById(R.id.txtTel1);
        txtTelefono2 = (EditText) findViewById(R.id.txtTel2);
        txtDireccion = (EditText) findViewById(R.id.txtDomicilio);
        txtNotas = (EditText) findViewById(R.id.txtNota);
        cbFavorito = (CheckBox) findViewById(R.id.chkFavorito);
        btnAgregar = (Button) findViewById(R.id.btnAgregar);
        btnListar = (Button) findViewById(R.id.btnListar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);

        db = new AgendaContactos(MainActivity.this);

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtNombre.getText().toString().matches("") || txtTelefono1.getText().toString().matches("")) {
                    Toast.makeText(MainActivity.this, "Ingrese nombre y teléfono 1 como mínimo", Toast.LENGTH_SHORT).show();
                } else { //Guardar o modificar
                    Contacto nContacto = new Contacto();
                    nContacto.setNombre(txtNombre.getText().toString());
                    nContacto.setDomicilio(txtDireccion.getText().toString());
                    nContacto.setNotas(txtNotas.getText().toString());
                    nContacto.setTelefono1(txtTelefono1.getText().toString());
                    nContacto.setTelefono2(txtTelefono2.getText().toString());
                    if(cbFavorito.isChecked()) fav = 1;
                    else fav = 0;
                    nContacto.setFavorito(fav);
                    db.openDataBase();
                    if(savedContact == null){
                        long idx = db.insertarContacto(nContacto);
                        Toast.makeText(MainActivity.this, "Se agregó el contacto con ID " + idx, Toast.LENGTH_SHORT).show();
                    }else{
                        db.actualizarContactos(nContacto,id);
                        Toast.makeText(MainActivity.this, "Se actualizó el registro " + id, Toast.LENGTH_SHORT).show();
                    }

                    db.closeDataBase();
                    limpiar();
                }
            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, listActivity.class);
                startActivityForResult(i, 0);
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if(Activity.RESULT_OK == resultCode) {
            Contacto contacto  = (Contacto)intent.getSerializableExtra("contacto");
            savedContact = contacto;
            id = contacto.get_ID();
            txtNombre.setText(contacto.getNombre());
            txtTelefono1.setText(contacto.getTelefono1());
            txtTelefono2.setText(contacto.getTelefono2());
            txtDireccion.setText(contacto.getDomicilio());
            txtNotas.setText(contacto.getNotas());
            if(contacto.getFavorito() == 1) cbFavorito.setChecked(true);
        }else{
            limpiar();
        }
    }

    public void limpiar(){
        txtNombre.setText("");
        txtDireccion.setText("");
        txtTelefono1.setText("");
        txtTelefono2.setText("");
        txtNotas.setText("");
        cbFavorito.setChecked(false);
        txtNombre.requestFocus();
    }

}
