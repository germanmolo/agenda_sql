package com.germanmolo.agenda_sql;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import database.AgendaContactos;
import database.Contacto;

public class listActivity extends android.app.ListActivity {

    private MyArrayAdapter adapter;
    private ArrayList<Contacto> listaContacto;
    private AgendaContactos agendaContactos;
    private Button btnNuevo;
    private ListView listaContactos;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_activity);
        btnNuevo = (Button)findViewById(R.id.btnNuevo);
        this.agendaContactos = new AgendaContactos(this);
        llenarLista();
        adapter = new MyArrayAdapter(this, R.layout.layout_contacto, listaContacto);
        String str = adapter.contactos.get(1).getNombre();
        setListAdapter(adapter);

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void llenarLista(){
        agendaContactos.openDataBase();
        listaContacto = agendaContactos.allContactos();
        agendaContactos.closeDataBase();
    }

    class MyArrayAdapter extends ArrayAdapter<Contacto>{
        private Context context;
        private int textViewResourceId;
        private ArrayList<Contacto> contactos;

        public MyArrayAdapter(@NonNull Context context, int textViewResourceId, ArrayList<Contacto> contactos) {
            super(context, textViewResourceId, contactos);
            this.context = context;;
            this.textViewResourceId = textViewResourceId;
            this.contactos = contactos;
        }

        public View getView(final int position, View converView, ViewGroup viewGroup){
            LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewResourceId, null);
            TextView lblNombre = (TextView)view.findViewById(R.id.lblNombre);
            TextView lblTelefono = (TextView)view.findViewById(R.id.lbltel1);

            Button btnModificar = (Button)view.findViewById(R.id.btnModificar);
            Button btnBorrar = (Button)view.findViewById(R.id.btnBorrar);

            if(contactos.get(position).getFavorito()>0){
                lblNombre.setTextColor((Color.BLUE));
                lblTelefono.setTextColor((Color.BLUE));
            }else{
                lblNombre.setTextColor((Color.BLACK));
                lblTelefono.setTextColor((Color.BLACK));
            }
            lblNombre.setText(contactos.get(position).getNombre());
            lblTelefono.setText(contactos.get(position).getTelefono1());

            btnBorrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    agendaContactos.openDataBase();
                    agendaContactos.deleteContacto(contactos.get(position).get_ID());
                    agendaContactos.closeDataBase();
                    contactos.remove(position);
                    notifyDataSetChanged();
                }
            });

            btnModificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("contacto", contactos.get(position));
                    Intent intent = new Intent();
                    intent.putExtras(bundle);
                    setResult(Activity.RESULT_OK,intent);
                    finish();
                }
            });

            return view;
        }
    }

}
